namespace Task3;

public class File
{
    private Directory dir;
    private String name;

    private String text = "";
    
    public File(String dirPath, String name)
    {
        this.dir = new Directory(dirPath);
        this.name = name;
    }

    public Directory Dir => dir;

    public string Name => name;

    public void Rename(String name)
    {
        this.name = name;
    }

    public void Create()
    {
        text = "";
    }

    public void Add(String s)
    {
        text += s + '\n';
    }

    public void Delete()
    {
        text = "";
    }

    public void Print()
    {
        Console.WriteLine(this.text);
    }
}