﻿// See https://aka.ms/new-console-template for more information

namespace Task3
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            var file = new TextFile("/somedir", "someFileName");
            file.Create();
            file.Add("Hello, world!");
            file.Add("My name is Lev");
            file.Print();
            file.Rename("someFileName2");
            Console.WriteLine(file.Name);
            file.Delete();
            file.Print();
        }
    }
}