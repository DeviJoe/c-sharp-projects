namespace Task3;

public class Directory
{
    private string name;
    
    public Directory(string name)
    {
        this.name = name;
    }

    public string Name => name;
}