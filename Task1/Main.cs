﻿// See https://aka.ms/new-console-template for more information

using System.Collections;
using System.Runtime.CompilerServices;

namespace Task1
{
    class MainClass
    {
        public static List<List<int>> ReadMatrixFromFile(String path)
        {
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    var res = new List<List<int>>();
                    while (!sr.EndOfStream)
                    {
                        var s = sr
                            .ReadLine()
                            ?.Split(' ')
                            .Select(x => Convert.ToInt32(x))
                            .ToList();
                        res.Add(s);
                    }
                    return res;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return null;
        }

        public static void deleteZeroRows(List<List<int>> s)
        {
            for (int j = 0; j < s.Count; j++) 
            {
                var isZero = true;
                foreach (var i in s[j])
                {
                    if (i != 0)
                    {
                        isZero = false;
                    }
                }

                if (isZero)
                {
                    s.Remove(s[j]);
                    j--;
                }
            }
        }

        public static void deleteZeroCols(List<List<int>> r)
        {
            for (int i = 0; i < r[0].Count; i++)
            {
                if (r[0][i] == 0)
                {
                    bool isZero = true;
                    for (int j = 0; j < r.Count; j++)
                    {
                        if (r[j][i] != 0) isZero = false;
                    }

                    if (isZero)
                    {
                        foreach (var ints in r)
                        {
                            ints.RemoveAt(i);
                        }
                    }
                }
            }
        }

        public static void WriteMatrixFromFile(String path, List<List<int>> s)
        {
            try
            {
                using (StreamWriter sr = new StreamWriter(path))
                {
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        /**
         * Вариант №10
         */
        public static void Main(string[] args)
        {
            var r = ReadMatrixFromFile("/Users/devijoe/RiderProjects/Tasks/Task1/input.txt");
            deleteZeroRows(r);
            deleteZeroCols(r);
            
            Console.WriteLine("=======\tРезультирующая матрица\t=======");
            foreach (var ints in r)
            {
                foreach (var i in ints)
                {
                    Console.Write(Convert.ToString(i) + '\t');
                }
                Console.WriteLine();
            }
        }
    }
}