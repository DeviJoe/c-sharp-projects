namespace Task5;

public class Comedy : RussianFilm
{
    private int funLovingRating;
    private bool isFunny = false;
    private String vote = "";
    
    public Comedy(string name) : base(name)
    {
        this.funLovingRating = this.Rating * 120 / 12;
        if (funLovingRating > 30)
        {
            this.isFunny = true;
        }
    }

    public int FunLovingRating => funLovingRating;

    public bool IsFunny => isFunny;

    public string Vote => vote;

    public void AddToCollect()
    {
        Console.WriteLine("Добавлен в коллекцию фильмов");
    }

    public override void CreateVote(string s)
    {
        Console.WriteLine("Составлена рецензия");
        this.vote = s;
    }
}