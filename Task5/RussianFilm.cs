namespace Task5;

public abstract class RussianFilm : Film
{
    private String name;
    private bool isStarred = false;
    private int rating = 0;
    private String vote = "";
    
    protected RussianFilm(string name)
    {
        this.name = name;
    }

    public string Name => name;

    public bool IsStarred => isStarred;

    public int Rating => rating;

    public void Watch()
    {
        Console.WriteLine("Фильм " + this.name + " поставлен в проигрыватель");
    }

    public void MakeStarred()
    {
        this.isStarred = true;
    }

    public void SetRaiting(int r)
    {
        this.rating = r;
    }

    public abstract void CreateVote(String s);
}