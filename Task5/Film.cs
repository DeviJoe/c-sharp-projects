namespace Task5;

public interface Film
{
    static String name;

    // Посмотреть
    void Watch();
    
    /*
     * Пометить любимым
     */
    void MakeStarred();
}