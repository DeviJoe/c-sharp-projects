﻿// See https://aka.ms/new-console-template for more information

namespace Task5
{
    public class MainClass
    {
        public static void Main(string[] args)
        {
            var films = new List<Film>();
            films.Add(new Comedy("Кавказская пленница"));
            films.Add(new Comedy("Джентельмены удачи!"));
            films.Add(new Comedy("Самогонщики"));
            
            films[0].MakeStarred();
            films[1].Watch();
        }
    }
}