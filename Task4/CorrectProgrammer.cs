namespace Task4;

public class CorrectProgrammer : Programmer
{
    private int correctProgCols;
    
    public CorrectProgrammer(string name, int programCols, int progLangCols, int correctProgCols) : base(name, programCols, progLangCols)
    {
        this.correctProgCols = correctProgCols;
    }

    public int CorrectProgCols => correctProgCols;
    
    public new int CheckQuality()
    {
        return ProgramCols * ProgLangCols / CorrectProgCols;
    } 
    
}