﻿// See https://aka.ms/new-console-template for more information

namespace Task4
{
    public class MainClass
    {
        public static void Main(string[] args)
        {
            CorrectProgrammer p1 = new CorrectProgrammer("Ivan Ivanov", 100, 3, 23);
            CorrectProgrammer p2 = new CorrectProgrammer("Fedor Fedorov", 153, 2, 11);
            CorrectProgrammer p3 = new CorrectProgrammer("Mary Danbro", 593, 1, 63);
            CorrectProgrammer p4 = new CorrectProgrammer("John Doe", 853, 5, 94);
            
            Console.WriteLine("Качество объекта 1 - " + p1.CheckQuality());
            Console.WriteLine("Качество объекта 2 - " + p2.CheckQuality());
            Console.WriteLine("Качество объекта 3 - " + p3.CheckQuality());
            Console.WriteLine("Качество объекта 4 - " + p4.CheckQuality());
        }
    }
}