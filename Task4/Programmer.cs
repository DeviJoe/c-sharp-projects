namespace Task4;

public class Programmer
{
    private String name;
    private int programCols;
    private int progLangCols;

    public Programmer(string name, int programCols, int progLangCols)
    {
        this.name = name;
        this.programCols = programCols;
        this.progLangCols = progLangCols;
    }

    public string Name => name;

    public int ProgramCols => programCols;

    public int ProgLangCols => progLangCols;

    public int CheckQuality()
    {
        return ProgramCols * ProgLangCols;
    }
}