﻿// See https://aka.ms/new-console-template for more information

namespace Task2
{
    class MainClass
    {
        public static void Task(string nameT, string nameS, string nameR)
        {
            try
            {
                using (StreamReader sr = new StreamReader(nameT))
                {
                    var text = new List<string>();
                    StreamWriter nameSFile = new StreamWriter(nameS, false);
                    StreamWriter nameRFile = new StreamWriter(nameR, false);
                    while (!sr.EndOfStream)
                    {
                        var line = sr.ReadLine();
                        var num = Convert.ToDouble(line?.Substring(60));
                        line = line.Substring(0, 60);
                        Console.WriteLine(line);
                        Console.WriteLine(num);

                        nameSFile.WriteLine(line);
                        nameRFile.WriteLine(num);
                    }
                    nameRFile.Close();
                    nameSFile.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        public static void Main(string[] args)
        {
            Task(
                "/Users/devijoe/RiderProjects/Tasks/Task2/NameT.txt", 
                "/Users/devijoe/RiderProjects/Tasks/Task2/NameS.txt", 
                "/Users/devijoe/RiderProjects/Tasks/Task2/NameR.txt");
        }
    }
}
